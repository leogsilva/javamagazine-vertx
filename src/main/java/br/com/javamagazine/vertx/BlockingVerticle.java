package br.com.javamagazine.vertx;

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.platform.Verticle;

public class BlockingVerticle extends Verticle {

  public void start() {
    final EventBus eventBus = vertx.eventBus();
    vertx.setPeriodic(100, new Handler<Long>() {
      public void handle(final Long timerID) {
        eventBus.send("block", 1);
      }
    });

    eventBus.registerHandler("block", new Handler<Message<String>>() {

      @Override
      public void handle(Message<String> message) {
        try {
          System.out.println(Thread.currentThread().getName());
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }

    });
  };
}
