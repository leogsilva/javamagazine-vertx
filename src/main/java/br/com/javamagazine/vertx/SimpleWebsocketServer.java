package br.com.javamagazine.vertx;

import java.util.Set;

import org.vertx.java.core.Handler;
import org.vertx.java.core.buffer.Buffer;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.http.ServerWebSocket;
import org.vertx.java.platform.Verticle;

public class SimpleWebsocketServer extends Verticle {

  public void start() {

    HttpServer server = vertx.createHttpServer();
    server.requestHandler(new Handler<HttpServerRequest>() {

      @Override
      public void handle(HttpServerRequest request) {
        request.response()
            .sendFile(
                '.' + (request.path().equals("/") ? "/index.html" : request
                    .path()));
      }

    });
    server.websocketHandler(new Handler<ServerWebSocket>() {

      @Override
      public void handle(final ServerWebSocket ws) {
        if (!ws.path().equals("/chat")) {
          ws.reject();
          return;
        }
        final Set<String> wsSessions = vertx.sharedData().getSet(
            "websocket.chat.sessions");
        wsSessions.add(ws.textHandlerID());

        ws.dataHandler(new Handler<Buffer>() {

          @Override
          public void handle(Buffer buffer) {
            for (String handlerId : wsSessions) {
              System.err.println("Sending " + handlerId);
              vertx.eventBus().send(handlerId, buffer.toString());
            }
          }

        });

        ws.closeHandler(new Handler<Void>() {

          @Override
          public void handle(Void v) {
            System.err.println("Socket closed. Removing" + ws.binaryHandlerID());
            wsSessions.remove(ws.textHandlerID());
          }

        });
      }

    });

    server.listen(8080);
  }
}
