package br.com.javamagazine.vertx;

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

public class FibonnaciWorker extends Verticle {

  @Override
  public void start() {
    final EventBus eventBus = vertx.eventBus();
    System.out.println("Registrando handler para fib.request");
    eventBus.registerHandler("fib.request", new Handler<Message<Integer>>() {

      @Override
      public void handle(Message<Integer> message) {
        Integer result = fib(message.body().intValue());
        System.out.println("Worker processing "
            + Thread.currentThread().getName());
        JsonObject resultMessage = new JsonObject();
        resultMessage.putString("number", message.body().toString());
        resultMessage.putString("result", result.toString());
        eventBus.send("fib.response", resultMessage);
      }

    });
  }

  private int fib(int number) {
    if (number == 0) {
      return 0;
    } else if (number == 1) {
      return 1;
    } else {
      return fib(number - 2) + fib(number - 1);
    }
  }
}
