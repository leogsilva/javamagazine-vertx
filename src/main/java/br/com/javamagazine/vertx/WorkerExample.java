package br.com.javamagazine.vertx;

import java.util.concurrent.atomic.AtomicBoolean;

import org.vertx.java.core.Handler;
import org.vertx.java.core.eventbus.EventBus;
import org.vertx.java.core.eventbus.Message;
import org.vertx.java.core.json.JsonObject;
import org.vertx.java.platform.Verticle;

/*
 * ~/javatools/vert.x-2.1.2/bin/vertx run br.com.javamagazine.vertx.WorkerExample -cp target/classes/ 
 */
public class WorkerExample extends Verticle {
  final AtomicBoolean canSend = new AtomicBoolean(true);

  public void start() {
    final EventBus eventBus = vertx.eventBus();

    Handler<Message<JsonObject>> resultHandler = new Handler<Message<JsonObject>>() {

      @Override
      public void handle(Message<JsonObject> message) {
        canSend.set(true);
        System.out.println(Thread.currentThread().getName() + "=>Fib("
            + message.body().getString("number") + ","
            + message.body().getString("result") + ")");
        eventBus.send("log.consumer", "Logging output "
            + message.body().getString("result"));
      }
    };

    eventBus.registerHandler("fib.response", resultHandler);

    vertx.setPeriodic(100, new Handler<Long>() {
      public void handle(final Long timerID) {
        System.out
            .println(Thread.currentThread().getName() + "=>Sending event");
        if (canSend.get()) {
          eventBus.send("fib.request", 1);
          canSend.set(false);
        }
      }
    });

    container.deployWorkerVerticle("br.com.javamagazine.vertx.FibonnaciWorker",
        10);
    container.deployVerticle("logconsumer.js");
    System.out.println("Iniciando requisicao");
  };

}
