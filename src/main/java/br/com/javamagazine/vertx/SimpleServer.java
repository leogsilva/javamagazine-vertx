package br.com.javamagazine.vertx;

import org.vertx.java.core.Handler;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.platform.Verticle;

public class SimpleServer extends Verticle {

  public void start() {
    vertx.createHttpServer().requestHandler(new Handler<HttpServerRequest>() {

      @Override
      public void handle(HttpServerRequest request) {
        String nome = request.params().get("nome");
        String message = String.format("Vertx manda noticias para %s", nome);
        request.response().setStatusCode(200).end(message);
      }

    }).listen(8080);
  }

}
