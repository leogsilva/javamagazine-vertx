package br.com.javamagazine.vertx;

import org.vertx.java.core.Handler;
import org.vertx.java.core.http.HttpServer;
import org.vertx.java.core.http.HttpServerRequest;
import org.vertx.java.core.http.RouteMatcher;
import org.vertx.java.platform.Verticle;

public class SimpleRouterServer extends Verticle {

  public void start() {
    HttpServer httpServer = vertx.createHttpServer();
    RouteMatcher routeMatcher = new RouteMatcher();
    routeMatcher.get("/ping", new Handler<HttpServerRequest>() {
      @Override
      public void handle(HttpServerRequest request) {
        request.response().end("pong");
      }
    });
    httpServer.requestHandler(routeMatcher).listen(8080);
  }
}
